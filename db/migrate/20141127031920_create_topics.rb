class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.integer :user_id
      t.string :name
      t.text :description
      t.integer :parent_id, :null => true, :index => true
      t.integer :lft, :null => false, :index => true
      t.integer :rgt, :null => false, :index => true

      # optional fields
      #t.integer :depth, :null => false
      #t.integer :children_count, :null => false


      t.timestamps
    end
    add_index :topics, :name, unique: true
  end
end
