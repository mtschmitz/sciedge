class CreateEdits < ActiveRecord::Migration
  def change
    create_table :edits do |t|
      t.integer :user_id
      t.integer :topic_id
      t.text :description
      t.string :chg
      t.timestamps
    end
  end
end
