json.array!(@solutions) do |solution|
  json.extract! solution, :user_id, :name, :description
  json.url solution_url(solution, format: :json)
end