
class Edit < ActiveRecord::Base
  acts_as_votable
  belongs_to :user
  belongs_to :topic
  
 # validates :user_id, presence: true
#  validates :description, presence: true, length: { maximum: 2000 }
  validates :topic_id, presence: true
  validates :chg, presence: true
    
  default_scope -> { order('created_at DESC') }

  def score
    self.get_upvotes.size - self.get_downvotes.size
  end
  
  def self.unsolved_feed
    #unsolved_ids = "SELECT name FROM problems WHERE user_id > 0" 
    #where("(#{unsolved_ids})")
    Edit.all
  end
end
