class Topic < ActiveRecord::Base
  acts_as_nested_set
  
  belongs_to :user
  has_many :change
  
  validates :name, presence: true
  validates :user_id, presence: true
  validates :description, presence: true, length: { maximum: 2000 }
#  make_voteable
    
  default_scope -> { order('created_at DESC') }


  def self.unsolved_feed
    #unsolved_ids = "SELECT name FROM problems WHERE user_id > 0" 
    #where("(#{unsolved_ids})")
    Topic.all
  end
  
  def self.short_collection_to_json()
    arr = self.short_collection_to_json_help()
    arr.to_json.html_safe
  end
  
   def self.short_collection_to_json_help(collection = self.roots)
    collection.inject([]) do |arr, model|
      if model.parent.nil?
        parent_name=nil
      else
        parent_name=model.parent.name
      end
      
      if model.children.empty?
         arr << { name: model.name, parent: parent_name, num: model.id  }
      else
         arr << { name: model.name, parent: parent_name, num: model.id, children: short_collection_to_json_help(model.children) }
      end
    end
  end
end




