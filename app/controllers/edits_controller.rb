require 'will_paginate/array'

class EditsController < ApplicationController
  #before_action :correct_user,   only: :destroy
  before_filter :authenticate_user!, :except => [:index, :show]
  respond_to :html, :js

  # GET /edits
  # GET /edits.json
  def index
    @feed_items = Edit.unsolved_feed.paginate(page: params[:page])
  end


  # GET /edits/1
  # GET /edits/1.json
  def show
  end
 

  # GET /edits/1/edit
  def edit
  end

  # POST /edits
  # POST /edits.json
  def create
    @edit = current_user.edits.build(edit_params)

    respond_to do |format|
      if @edit.save
        format.html { redirect_to root_path, notice: 'edit was successfully created.' }
        format.json { render action: 'show', status: :created, location: @edit }
      else
        format.html { render action: 'new' }
        format.json { render json: @edit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /edits/1
  # DELETE /edits/1.json
  def destroy
    @edit.destroy
    respond_to do |format|
      format.html { redirect_to edits_url }
      format.json { head :no_content }
    end
  end
  
  def upvote
    @edit = Edit.find(params[:id])
    @edit.upvote_from current_user
    if @edit.score > 3
      if @edit.chg == "DELETE"
        @edit.topic.destroy
      end
    end
    redirect_to edits_path
        respond_to do |format|
          format.html{ redirect_to edits_url }
          format.js
        end      
  end
  
  def downvote
    @edit = Edit.find(params[:id])
    @edit.downvote_from current_user
    if @edit.score < -3
      @edit.destroy
    end
    redirect_to edits_path
        respond_to do |format|
          format.html{ redirect_to edits_url }
          format.js
        end
  end
  
  
  private
    # Use callbacks to share common setup or constraints between actions.


    # Never trust parameters from the scary internet, only allow the white list through.
    def edit_params
      params.require(:topic).permit(:user_id, :chg , :topic_id, :topic, :description)
    end
    
   def correct_user
      @edit = current_user.edits.find_by(id: params[:id])
      redirect_to root_url if @edit.nil?
    end
end
