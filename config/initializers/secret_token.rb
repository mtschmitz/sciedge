# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
EdgeOfScience::Application.config.secret_key_base = 'd71f34a55953022a6a6cb2aab82bbed52ed2a4bc6fe0316a13257debd6001e4ab01411a500f7ca6059d3d11bf3df817abaf013cccfa55c2e66dec897c3bb2bd0'
